<!doctype html>
<html lang="en">
<head>
	<base href="<?=base_url()?>">
	<meta charset="UTF-8">
	<title>CRUD</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container">

		<form method="post" action="<?php echo base_url('data/aksi_login'); ?>" method="post" class="form-horizontal">

			<div class="form-group">
				<label class="control-label col-sm-2">
					E-mail
				</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" name="email">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">
					Password
				</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password">
				</div>
			</div>
			

			<button name="tombol_submit" class="btn btn-primary" type="submit" value="Login">
			LOGIN</button>

			<a href="data/add" class="btn btn-primary">Sign-Up</a>


		</form>
	</div>
</body>
</html>